#!/bin/bash

SIMPLE_MONITOR_VERSION="0.20.0-SNAPSHOT"

IMAGE_NAME="zeebe-simple-monitor"
REPOSITORY="nikolaypervukhin"

./mvnw clean package

rm ./container/target -R
mkdir ./container/target
mv "./target/zeebe-simple-monitor-$SIMPLE_MONITOR_VERSION.jar" ./container/target/simple-monitor.jar

docker build -t $IMAGE_NAME ./container
docker tag $IMAGE_NAME $REPOSITORY/$IMAGE_NAME
docker login --username $DOCKER_LOGIN --password $DOCKER_PASSWORD
docker push $REPOSITORY/$IMAGE_NAME